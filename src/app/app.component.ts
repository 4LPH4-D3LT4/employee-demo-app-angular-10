import { Component } from '@angular/core';
import { EmployeeModel } from './model/EmployeeModel';
import { EmployeeService } from './employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(private employeeService: EmployeeService) {}
  title = 'employee-form-application';
  public firstName: string = '';
  public middleName: string = '';
  public lastName: string = '';
  public address: string = '';
  public mobileNumber: number;
  public email:string = '';

  public onSubmit() {
    const employee = new EmployeeModel();
    employee.firstName = this.firstName;
    employee.middleName = this.middleName;
    employee.lastName = this.lastName;
    employee.address=this.address;
    employee.mobileNumber = this.mobileNumber.toString();
    employee.email = this.email;
    this.employeeService.addEmployee(employee).subscribe(
      (data) => console.log('Success!', data),
      (error) => console.log('Error!', error)
    );
  }
}
