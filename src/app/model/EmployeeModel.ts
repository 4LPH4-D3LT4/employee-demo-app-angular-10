export class EmployeeModel {
  public firstName: string;
  public lastName: string;
  public middleName: string;
  public address: string;
  public mobileNumber: string;
  public email: string;
}
