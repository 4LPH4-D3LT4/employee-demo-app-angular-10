import { Injectable } from '@angular/core';
import { EmployeeModel } from './model/EmployeeModel';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor(private httpClient: HttpClient) {}
  _url = 'http://localhost:8080/api/employee/join';
  addEmployee(employee: EmployeeModel) {
    return this.httpClient.post<any>(this._url, employee);
  }
}
